package main;

import model.Car;
import service.CarService;
import java.util.*;

public class Main {

    public static List<Car> cars = new ArrayList<>();

    public static void main(String[] args) {

        CarService carService = new CarService();

        Car car1 = new Car("Fiat", 30000);
        Car car2 = new Car("Toyota", 10000);
        Car car3 = new Car("Volvo", 20000);
        Car car4 = new Car("Skoda", 40000);
        Car car5 = new Car("BMW", 50000);
        Car car6 = new Car("Tesla", 60000);
        Car car7 = new Car("Mercedes", 70000);
        Car car8 = new Car("Ferrari", 80000);

        carService.addCarsToList(car1);
        carService.addCarsToList(car2);
        carService.addCarsToList(car3);
        carService.addCarsToList(car4);
        carService.addCarsToList(car5);
        carService.addCarsToList(car6);
        carService.addCarsToList(car7);
        carService.addCarsToList(car8);

        carService.sortByName(cars);

        carService.sortByNameReverse(cars);

        carService.sortByPriceReverse(cars);

        final Car bmw = carService.searchByName("BMW");
        System.out.println("Search by name " + bmw);

        carService.getCarsWherePriceGratherThan(30000).forEach(car -> System.out.println("List with cars where price is grather than 3000: " + car));

        carService.printAllCars();

    }

}