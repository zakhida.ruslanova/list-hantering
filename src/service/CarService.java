package service;

import model.Car;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import static main.Main.cars;

public class CarService implements CarServiceI {

    public List<Car> addCarsToList(Car car) {
        cars.add(car);
        return cars;
    }

    public void sortByName(List<Car> cars) {
        cars.sort(null);
        for (Car s : cars)
            System.out.println("After sorting by name: " + "The " + s.getName() + " has price " + s.getPrice());
    }

    public void sortByNameReverse(List<Car> cars) {
        Collections.reverse(cars);
        for (Car s : cars)
            System.out.println("After sorting reverse by name: " + "The " + s.getName() + " has price " + s.getPrice());
    }

    public void sortByPriceReverse(List<Car> cars) {
        cars.sort(Comparator.comparingInt(Car::getPrice)
                .reversed());
        for (Car s : cars)
            System.out.println("After sorting reverse by price: " + "The " + s.getName() + " has price " + s.getPrice());
    }

    public Car searchByName(String name) {
        return cars.stream().filter(car -> car.getName().equals(name)).collect(Collectors.toList()).get(0);
    }

    public List<Car> getCarsWherePriceGratherThan(int price) {
        return cars.stream().filter(car -> car.getPrice() > price).collect(Collectors.toList());
    }

    public void printAllCars() {
        cars.forEach(car -> System.out.println("Print all cars " + car));
    }
}
