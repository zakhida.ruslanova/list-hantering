package service;

import model.Car;
import java.util.List;

public interface CarServiceI {

    List<Car> addCarsToList(Car car);

    void sortByName(List<Car> cars);

    void sortByNameReverse(List<Car> cars);

    void sortByPriceReverse(List<Car> cars);

    Car searchByName(String name);

    List<Car> getCarsWherePriceGratherThan(int price);

    void printAllCars();

}
